import React from 'react';
import { Route, Switch,Redirect } from 'react-router-dom';
import Moblie  from '../src/Screens/Moblie';
import Email  from '../src/Screens/Email';
import Payment  from '../src/Screens/Payment';
import Delivery  from '../src/Screens/Delivery';
import RegisterSign  from '../src/Screens/RegisterSign';
import Topheader  from '../src/Components/TopHeader/Topheader';
import Navbar from '../src/Components/MainHeader/Navbar'
import Home  from '../src/Screens/Home';
import Heart  from '../src/Screens/Heart';
import Card  from '../src/Screens/Card';
import About  from '../src/Screens/About';
import Contact  from '../src/Screens/Contact';
import Blog  from '../src/Screens/Blog';
import SportsShoes  from '../src/Screens/SportsShoes';
import ProductsCards  from '../src/Screens/ProductsCards';
import './index.css';
import Categories from '../src/Components/CategoriesHeader/Categories'
import Responsiveheader from '../src/Components/ResponsiveHeader/Responsiveheader'
import SubMenu from '../src/Screens/SubMenu'

const App = ()=>{
  return (
    <React.Fragment>
      <Topheader />
      <Navbar />
      <Categories />
      <Responsiveheader />
        <Switch>
          <Route exact  path='/moblie' component={Moblie} />  
          <Route exact path='/email' component={Email} /> 
          <Route exact path='/payment' component={Payment} /> 
          <Route exact path='/delivery' component={Delivery} /> 
          <Route exact path='/registersign' component={RegisterSign} /> 
          <Route exact path="/" component={Home}/>
          <Route path="/about" component={About} />
          <Route path="/blog" component={Blog} />
          <Route path="/contact" component={Contact} />
          <Route exact path="/heart" component={Heart}/>
          <Route exact path="/card" component={Card} />
          <Route exact path="/sportsshoes" component={SportsShoes} />
          <Route exact path="/productscards" component={ProductsCards} />
          <Route exact path="/subMenu" component={SubMenu} />
          <Redirect to='/'/>
        </Switch>
    </React.Fragment>
    )
}

export default App;

// <Route exact path="/" component={Home} />
// <Route path="/about" component={About} />
// <Route path="/blog" component={Blog} />
// <Route path="/contact" component={Contact} />
