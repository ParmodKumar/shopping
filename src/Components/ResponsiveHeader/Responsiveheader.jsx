import React, { useState } from "react";
import CloseIcon from "@material-ui/icons/Close";
import { NavLink } from "react-router-dom";
import shoppinglogo from "../Images/shoppinglogo.svg";
import MenuIcon from "@material-ui/icons/Menu";
import "../ResponsiveHeader/Responsiveheader.css";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import HomeIcon from "@material-ui/icons/Home";
import PersonIcon from "@material-ui/icons/Person";
import FormatBoldIcon from "@material-ui/icons/FormatBold";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import PaymentOutlinedIcon from "@material-ui/icons/PaymentOutlined";
import LocalShippingIcon from "@material-ui/icons/LocalShipping";
import BorderAllIcon from "@material-ui/icons/BorderAll";
const Responsiveheader = () => {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);
  return (
    <React.Fragment>
      <div className="container-fluid">
        <nav className="navbar-sec">
          <div className="nav-container">
            <NavLink exact to="/" className="nav-logo">
              <img src={shoppinglogo} alt="shoppinglogo" className="logoimg" />
              Shopping
            </NavLink>

            <ul className={click ? "nav-menu active" : "nav-menu"}>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <HomeIcon className="menu-icon" />
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/about"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <PersonIcon className="menu-icon" />
                  About us
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/blog"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <FormatBoldIcon className="menu-icon" />
                  Blog
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/contact"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <MailOutlineIcon className="menu-icon" /> Contact Us
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/card"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <AddShoppingCartIcon className="menu-icon" />
                  Shopping Cart
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/registersign"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <PersonAddIcon className="menu-icon" />
                  Register
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/payment"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <PaymentOutlinedIcon className="menu-icon" />
                  Payment
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  exact
                  to="/delivery"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <LocalShippingIcon className="menu-icon" />
                  Delivery
                </NavLink>
              </li>
              <li className="nav-item">
                <h3
                  exact
                  to="/subMenu"
                  activeClassName="active"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  <BorderAllIcon className="menu-icon" />
                  All Category
                </h3>
              </li>
            </ul>
            <div className="nav-icon" onClick={handleClick}>
              {click ? (
                <CloseIcon className="nav-icon" />
              ) : (
                <MenuIcon className="nav-icon" />
              )}
            </div>
          </div>
          <NavLink className="nav-link" to="/card">
            <ShoppingCartOutlinedIcon className="icon-sec" />
          </NavLink>
        </nav>
      </div>
    </React.Fragment>
  );
};
export default Responsiveheader;
