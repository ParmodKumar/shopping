import React from 'react';
import {NavLink} from 'react-router-dom';

const MegaMenu = (props)=>{
    return (
        <React.Fragment>
        <div className="column">
                <div className='mega-menu-main-hdng'>
                    <h5>{props.title}</h5>
                </div>
                <div className='mega-menu-list'>
                    <ul>
                        <li>
                            <NavLink to={props.Screens}>{props.list}</NavLink>
                        </li>
                    </ul>
                </div>
                </div>
        </React.Fragment>
    )  
}
export default MegaMenu;
