import React from 'react';
import {NavLink} from 'react-router-dom';
import '../CategoriesHeader/Categories.css'
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import MegaMenu from '../CategoriesHeader/MegaMenu'
import AllCategories from '../CategoriesHeader/AllCategories'

const Categories = ()=>{
    return (
        <React.Fragment>
        <div className='container-fluid'>
        <nav>
            <div className='bottom-navbar navbar navbar-expand-sm p-0'>
                <div className='all-categories-list'>
                    <ul className="navbar-nav all-categories-list-ul">
                        <li className="nav-item dropdown">
                            <NavLink to='sportsshoes' id="dropdownMenuButton" data-toggle="dropdown"><FormatAlignLeftIcon className='MenuIcon'/>All Categories</NavLink>
                            <div className="dropdown-menu all-ctgre-drp-menu" aria-labelledby="dropdownMenuButton">
                                <ul>
                                    <AllCategories 
                                    Screen='/sportsshoes'
                                    title='Sports Shoes'
                                    />
                                    <AllCategories 
                                    Screen='/sportsshoes'
                                    title='Sports Shoes'
                                    />
                                    <AllCategories 
                                    Screen='/sportsshoes'
                                    title='Sports Shoes'
                                    />
                                    <AllCategories 
                                    Screen='/sportsshoes'
                                    title='Sports Shoes'
                                    />
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className='bottom-nav-tap-menu'>
                    <ul className='un-order-list'>
                        <li className='subnav'>
                            <NavLink className='productscards' to="/productscards">T-shirt</NavLink>
                            <div  className='subnav-content'>
                                <MegaMenu 
                                Screens="/productscards"
                                title='T-shirt 1'
                                list='Products Cards 1'
                                />
                                <MegaMenu 
                                Screens="/productscards"
                                title='T-shirt 1'
                                list='Products Cards 2'
                                />
                                <MegaMenu 
                                Screens="/productscards"
                                title='T-shirt 1'
                                list='Products Cards 2'
                                />
                            </div> 
                            </li>
                        <li className='subnav'>
                            <NavLink className='subnavbtn' to="/productscards">Shirts</NavLink>
                            <div  className='subnav-content'>
                                <div className="column">
                                    <div className='mega-menu-main-hdng'>
                                        <h5>Shirts List 1</h5>
                                    </div>
                                    <div className='mega-menu-list'>
                                        <ul>
                                            <li>
                                                <NavLink to="/productscards">Products Cards2</NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className='subnav'>
                            <NavLink className='subnavbtn' to="/productscards">Formal Shirts</NavLink>
                            <div  className='subnav-content'>
                                <div className="column">
                                    <div className='mega-menu-main-hdng'>
                                        <h5>Shirts List 1</h5>
                                    </div>
                                    <div className='mega-menu-list'>
                                        <ul>
                                            <li>
                                                <NavLink to="/productscards">Products Cards3</NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className='subnav'>
                            <NavLink className='subnavbtn' to="/productscards">Casual Shirts</NavLink>
                            <div  className='subnav-content'>
                                <div className="column">
                                    <div className='mega-menu-main-hdng'>
                                        <h5>Shirts List 1</h5>
                                    </div>
                                    <div className='mega-menu-list'>
                                        <ul>
                                            <li>
                                                <NavLink to="/productscards">Products Cards4</NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className='subnav'>
                            <NavLink className='subnavbtn' to="/productscards">Jeans</NavLink>
                            <div  className='subnav-content'>
                                <div className="column">
                                    <div className='mega-menu-main-hdng'>
                                        <h5>Shirts List 1</h5>
                                    </div>
                                    <div className='mega-menu-list'>
                                        <ul>
                                            <li>
                                                <NavLink to="/productscards">Products Cards5</NavLink>
                                            </li>
                                         
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className='subnav'>
                            <NavLink className='subnavbtn' to="/productscards">Shorts</NavLink>
                            <div  className='subnav-content'>
                                <div className="column">
                                    <div className='mega-menu-main-hdng'>
                                        <h5>Shirts List 1</h5>
                                    </div>
                                    <div className='mega-menu-list'>
                                        <ul>
                                            <li>
                                                <NavLink to="/productscards">Products Cards6</NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li className='subnav'>
                        <NavLink className='subnavbtn' to="/productscards">Cargos</NavLink>
                        <div  className='subnav-content'>
                            <div className="column">
                                <div className='mega-menu-main-hdng'>
                                    <h5>Shirts List 1</h5>
                                </div>
                                <div className='mega-menu-list'>
                                    <ul>
                                        <li>
                                            <NavLink to="/productscards">Products Cards7</NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li className='subnav'>
                    <NavLink className='subnavbtn' to="/productscards">Track pants</NavLink>
                    <div  className='subnav-content'>
                        <div className="column">
                            <div className='mega-menu-main-hdng'>
                                <h5>Shirts List 1</h5>
                            </div>
                            <div className='mega-menu-list'>
                                <ul>
                                    <li>
                                        <NavLink to="/productscards">Products Cards8</NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li className='subnav'>
                <NavLink className='subnavbtn' to="/productscards">Ties</NavLink>
                <div  className='subnav-content'>
                    <div className="column">
                        <div className='mega-menu-main-hdng'>
                            <h5>Shirts List 1</h5>
                        </div>
                        <div className='mega-menu-list'>
                            <ul>
                                <li>
                                    <NavLink to="/productscards">Products Cards9</NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
                    </ul>
                </div>

            </div>
            </nav>
            </div>
        </React.Fragment>
    )  
}
export default Categories;