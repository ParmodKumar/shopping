import React from 'react';
import {NavLink} from 'react-router-dom';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const AllCategories = (props)=>{
    return (
        <React.Fragment>
        <li className="nav-item">
        <NavLink  to={props.Screen} className="nav-link">
           {props.title}
            <ChevronRightIcon className='ChevronRightIcon'/>
        </NavLink>                     
        </li>
        </React.Fragment>
    )  
}
export default AllCategories;

