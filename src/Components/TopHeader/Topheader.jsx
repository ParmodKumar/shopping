import React from 'react';
import { NavLink } from 'react-router-dom';
import CallIcon from '@material-ui/icons/Call';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PaymentIcon from '@material-ui/icons/Payment';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import '../TopHeader/Topheader.css'
const  Topheader = ()=>{
  
    return( 
        <React.Fragment>
        <nav>
        <div className='nav-bar'>
            <div className='top-navbar navbar navbar-expand-sm p-0 pt-1' >
                <div className='container-fluid'>
                    <div className='nav-left-menu'>
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <NavLink className="nav-link pr-0 pl-0" to='/moblie'>
                                <CallIcon className="material-icons"/><span>(+800) 123 456 7890</span></NavLink>
                            </li>
                        <li className="nav-item">
                            <NavLink className="nav-link pr-0 pl-0" to='/email'><MailOutlineIcon className="material-icons"/><span>manager@shop.com</span></NavLink>
                        </li>
                    </ul>
                </div>
                <div className='nav-right-menu'>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link pr-0 pl-0" to='/payment'><PaymentIcon className="material-icons"/><span>Payment</span></NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link pr-0 pl-0" to='/delivery'><LocalShippingIcon className="material-icons"/><span>Delivery</span></NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link pr-0 pl-0" to='/registersign'><PersonOutlineIcon className="material-icons"/><span>Register or Sign in</span></NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </nav>
        </React.Fragment>
    )
}
export default Topheader;


