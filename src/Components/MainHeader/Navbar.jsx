import React from 'react';
import { NavLink } from 'react-router-dom';
import shoppinglogo from '../Images/shoppinglogo.svg'
import profile from '../Images/profile.png'

import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import FavoriteBorderOutlinedIcon from '@material-ui/icons/FavoriteBorderOutlined';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import '../MainHeader/Navbar.css'
import { makeStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
const Navbar = ()=>{
 
    return (
        <React.Fragment>
            <div className='container-fluid'>
            <nav>
            <div className='midle-navbar'>
            <div className='navbar navbar-expand-sm p-0'>
                <div className='logo-sec'>
                <NavLink className="navbar-brand active" to="/">
                <img src={shoppinglogo} alt='shoppinglogo' className='logoimg'/>Shopping</NavLink>
                </div>
                <div className='nav-dscrpstn'>
                <p>Description of your <br /> online store</p>
            </div>
                <div className='nav-search-bar'>
                        <div className='sreach-bar'>
                            <input className="form-control search-input" type="search" placeholder="Search" aria-label="Search" />
                            <button className="btn my-2 my-sm-0 search-btn d-flex" type="submit">Search<SearchOutlinedIcon className='searchicon'/></button>
                        </div>
                    </div>
            <div className='midle-rigth-menu'>
              <ul className="navbar-nav ml-auto">
                <li className="nav-item iconsec">
                  <NavLink className="nav-link" to='/heart'>
                  <Badge  badgeContent={4}>
                  <FavoriteBorderOutlinedIcon className='icon'/>
                  </Badge>
                  </NavLink>

                  </li>
                <li className="nav-item iconsec">
                  <NavLink className="nav-link" to='/card'>
                  <Badge badgeContent={4}>
                  <ShoppingCartOutlinedIcon className='icon'/>
                  </Badge>
                  </NavLink>
                 
                  </li>
       
                
              </ul>
            </div>
            </div>
            </div>
            
            </nav>
            </div>
        </React.Fragment>
    )  
}
export default Navbar;


// <nav>
// <div className='midle-navbar'>
// <div className='navbar navbar-expand-sm p-0'>
//     <div className='logo-sec'>
//     <NavLink className="navbar-brand" to="/"><img src={shoppinglogo} alt='shoppinglogo' className='logoimg'/>Shopping</NavLink>
//     </div>
//     <div className='nav-dscrpstn'>
//     <p>Description of your <br /> online store</p>
// </div>
//     <div className='nav-search-bar'>
//             <div className='sreach-bar'>
//                 <input className="form-control search-input" type="search" placeholder="Search" aria-label="Search" />
//                 <button className="btn my-2 my-sm-0 search-btn d-flex" type="submit">Search<SearchOutlinedIcon className='searchicon'/></button>
//             </div>
//         </div>
// <div className='midle-rigth-menu'>
//   <ul className="navbar-nav ml-auto">
//     <li className="nav-item iconsec">
//       <NavLink className="nav-link" to='/heart'><FavoriteBorderOutlinedIcon className='icon'/></NavLink>
//       <span className='msg-notification'>13</span>
//       </li>
//     <li className="nav-item iconsec">
//       <NavLink className="nav-link" to='/card'><ShoppingCartOutlinedIcon className='icon'/></NavLink>
//       <span className='msg-notification'>10</span>
//       </li>
//     <div className='card-pay'>
//     <p>My cart<br />$0.00</p>
// </div>
//   </ul>
// </div>
// </div>
// </div>
// </nav>



// <ul class="navbar-nav ml-auto">
// <li className="nav-item iconsec">
// <NavLink className="nav-link" to='/heart'><FavoriteBorderOutlinedIcon className='icon'/></NavLink>
// <span className='msg-notification'>13</span>
// </li>
// <li className="nav-item iconsec">
// <NavLink className="nav-link" to='/card'><ShoppingCartOutlinedIcon className='icon'/></NavLink>
// <span className='msg-notification'>10</span>
// </li>
// <div className='card-pay'>
// <p>My cart<br />$0.00</p>
// </div>
// </ul>



// <nav className="navbar-sec">
// <div className="nav-container">

//   <NavLink exact to="/" className="nav-logo">
//   <img src={shoppinglogo} alt='shoppinglogo' className='logoimg'/>Shopping
//   </NavLink>
  

  
//   <ul className={click ? "nav-menu active" : "nav-menu"}>
//     <li className="nav-item">
//       <NavLink
//         exact
//         to="/"
//         activeClassName="active"
//         className="nav-links"
//         onClick={closeMobileMenu}
//       >
//         Home
//       </NavLink>
//     </li>
//     <li className="nav-item">
//       <NavLink
//         exact
//         to="/about"
//         activeClassName="active"
//         className="nav-links"
//         onClick={closeMobileMenu}
//       >
//         About
//       </NavLink>
//     </li>
//     <li className="nav-item">
//       <NavLink
//         exact
//         to="/blog"
//         activeClassName="active"
//         className="nav-links"
//         onClick={closeMobileMenu}
//       >
//         Blog
//       </NavLink>
//     </li>
//     <li className="nav-item">
//       <NavLink
//         exact
//         to="/contact"
//         activeClassName="active"
//         className="nav-links"
//         onClick={closeMobileMenu}
//       >
//         Contact Us
//       </NavLink>
//     </li>
//   </ul>
//   <div className="nav-icon" onClick={handleClick}>
//   {click ? (
//    <CloseIcon className="nav-icon"/>
//   ) : (
//       <MenuIcon  className="nav-icon" />
//   )}
//   </div>

  
// </div>
// </nav>